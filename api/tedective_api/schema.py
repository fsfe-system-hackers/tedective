# SPDX-FileCopyrightText: 2023 Free Software Foundation Europe <contact@fsfe.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later

"""
Title: TEDective
Description: TEDective schema
"""
from datetime import datetime
from typing import Any, Dict, List, Optional, Set, Union

from terminusdb_client.woqlschema import (
    DocumentTemplate,
    EnumTemplate,
    HashKey,
    RandomKey,
    TaggedUnion,
)


class Country(DocumentTemplate):
    """
    This is Country.
    Country is a class object in the schema. It's class attributes will be the properties of the object. Therefore, a Country object will have a name which is string and a list of alias names that is called 'also_know_as'
    """

    _key = HashKey(["name"])
    also_know_as: List[str]
    name: str


class Address(DocumentTemplate):
    """This is address"""

    # commented out _subdocument = []
    country: Optional["Country"]
    postal_code: Optional[str]
    street: Optional[str]


class Coordinate(DocumentTemplate):
    # _key = RandomKey()
    _abstract = []
    x: float
    y: float


class InitiationType(EnumTemplate):
    tender = ()


class Status(EnumTemplate):
    planning = ()
    planned = ()
    active = ()
    cancelled = ()
    unsuccessful = ()
    complete = ()
    withdrawn = ()
    NoneType_None = ()


class Location(Address, Coordinate):
    """Location is inherits from Address and Coordinate
    Class can have multiple inheritance. It will inherits both the attibutes from Address and Coordinate.
    """

    _subdocument = []
    country: "Country"
    name: str
    postal_code: str
    street: str
    x: float
    y: float


class Tag(EnumTemplate):
    planning = ()
    planningUpdate = ()
    tender = ()
    tenderAmendment = ()
    tenderUpdate = ()
    tenderCancellation = ()
    award = ()
    awardUpdate = ()
    awardCancellation = ()
    contract = ()
    contractUpdate = ()
    contractAmendment = ()
    implementation = ()
    implementationUpdate = ()
    contractTermination = ()
    compiled = ()


class Currency(EnumTemplate):
    ADP = "ADP"
    AED = "AED"
    AFA = "AFA"
    AFN = "AFN"
    ALK = "ALK"
    ALL = "ALL"
    AMD = "AMD"
    ANG = "ANG"
    AOA = "AOA"
    AOK = "AOK"
    AON = "AON"
    AOR = "AOR"
    ARA = "ARA"
    ARP = "ARP"
    ARS = "ARS"
    ARY = "ARY"
    ATS = "ATS"
    AUD = "AUD"
    AWG = "AWG"
    AYM = "AYM"
    AZM = "AZM"
    AZN = "AZN"
    BAD = "BAD"
    BAM = "BAM"
    BBD = "BBD"
    BDT = "BDT"
    BEC = "BEC"
    BEF = "BEF"
    BEL = "BEL"
    BGJ = "BGJ"
    BGK = "BGK"
    BGL = "BGL"
    BGN = "BGN"
    BHD = "BHD"
    BIF = "BIF"
    BMD = "BMD"
    BND = "BND"
    BOB = "BOB"
    BOP = "BOP"
    BOV = "BOV"
    BRB = "BRB"
    BRC = "BRC"
    BRE = "BRE"
    BRL = "BRL"
    BRN = "BRN"
    BRR = "BRR"
    BSD = "BSD"
    BTN = "BTN"
    BUK = "BUK"
    BWP = "BWP"
    BYB = "BYB"
    BYN = "BYN"
    BYR = "BYR"
    BZD = "BZD"
    CAD = "CAD"
    CDF = "CDF"
    CHC = "CHC"
    CHE = "CHE"
    CHF = "CHF"
    CHW = "CHW"
    CLF = "CLF"
    CLP = "CLP"
    CNY = "CNY"
    COP = "COP"
    COU = "COU"
    CRC = "CRC"
    CSD = "CSD"
    CSJ = "CSJ"
    CSK = "CSK"
    CUC = "CUC"
    CUP = "CUP"
    CVE = "CVE"
    CYP = "CYP"
    CZK = "CZK"
    DDM = "DDM"
    DEM = "DEM"
    DJF = "DJF"
    DKK = "DKK"
    DOP = "DOP"
    DZD = "DZD"
    ECS = "ECS"
    ECV = "ECV"
    EEK = "EEK"
    EGP = "EGP"
    ERN = "ERN"
    ESA = "ESA"
    ESB = "ESB"
    ESP = "ESP"
    ETB = "ETB"
    EUR = "EUR"
    FIM = "FIM"
    FJD = "FJD"
    FKP = "FKP"
    FRF = "FRF"
    GBP = "GBP"
    GEK = "GEK"
    GEL = "GEL"
    GHC = "GHC"
    GHP = "GHP"
    GHS = "GHS"
    GIP = "GIP"
    GMD = "GMD"
    GNE = "GNE"
    GNF = "GNF"
    GNS = "GNS"
    GQE = "GQE"
    GRD = "GRD"
    GTQ = "GTQ"
    GWE = "GWE"
    GWP = "GWP"
    GYD = "GYD"
    HKD = "HKD"
    HNL = "HNL"
    HRD = "HRD"
    HRK = "HRK"
    HTG = "HTG"
    HUF = "HUF"
    IDR = "IDR"
    IEP = "IEP"
    ILP = "ILP"
    ILR = "ILR"
    ILS = "ILS"
    INR = "INR"
    IQD = "IQD"
    IRR = "IRR"
    ISJ = "ISJ"
    ISK = "ISK"
    ITL = "ITL"
    JMD = "JMD"
    JOD = "JOD"
    JPY = "JPY"
    KES = "KES"
    KGS = "KGS"
    KHR = "KHR"
    KMF = "KMF"
    KPW = "KPW"
    KRW = "KRW"
    KWD = "KWD"
    KYD = "KYD"
    KZT = "KZT"
    LAJ = "LAJ"
    LAK = "LAK"
    LBP = "LBP"
    LKR = "LKR"
    LRD = "LRD"
    LSL = "LSL"
    LSM = "LSM"
    LTL = "LTL"
    LTT = "LTT"
    LUC = "LUC"
    LUF = "LUF"
    LUL = "LUL"
    LVL = "LVL"
    LVR = "LVR"
    LYD = "LYD"
    MAD = "MAD"
    MDL = "MDL"
    MGA = "MGA"
    MGF = "MGF"
    MKD = "MKD"
    MLF = "MLF"
    MMK = "MMK"
    MNT = "MNT"
    MOP = "MOP"
    MRO = "MRO"
    MRU = "MRU"
    MTL = "MTL"
    MTP = "MTP"
    MUR = "MUR"
    MVQ = "MVQ"
    MVR = "MVR"
    MWK = "MWK"
    MXN = "MXN"
    MXP = "MXP"
    MXV = "MXV"
    MYR = "MYR"
    MZE = "MZE"
    MZM = "MZM"
    MZN = "MZN"
    NAD = "NAD"
    NGN = "NGN"
    NIC = "NIC"
    NIO = "NIO"
    NLG = "NLG"
    NOK = "NOK"
    NPR = "NPR"
    NZD = "NZD"
    OMR = "OMR"
    PAB = "PAB"
    PEH = "PEH"
    PEI = "PEI"
    PEN = "PEN"
    PES = "PES"
    PGK = "PGK"
    PHP = "PHP"
    PKR = "PKR"
    PLN = "PLN"
    PLZ = "PLZ"
    PTE = "PTE"
    PYG = "PYG"
    QAR = "QAR"
    RHD = "RHD"
    ROK = "ROK"
    ROL = "ROL"
    RON = "RON"
    RSD = "RSD"
    RUB = "RUB"
    RUR = "RUR"
    RWF = "RWF"
    SAR = "SAR"
    SBD = "SBD"
    SCR = "SCR"
    SDD = "SDD"
    SDG = "SDG"
    SDP = "SDP"
    SEK = "SEK"
    SGD = "SGD"
    SHP = "SHP"
    SIT = "SIT"
    SKK = "SKK"
    SLL = "SLL"
    SOS = "SOS"
    SRD = "SRD"
    SRG = "SRG"
    SSP = "SSP"
    STD = "STD"
    STN = "STN"
    SUR = "SUR"
    SVC = "SVC"
    SYP = "SYP"
    SZL = "SZL"
    THB = "THB"
    TJR = "TJR"
    TJS = "TJS"
    TMM = "TMM"
    TMT = "TMT"
    TND = "TND"
    TOP = "TOP"
    TPE = "TPE"
    TRL = "TRL"
    TRY = "TRY"
    TTD = "TTD"
    TWD = "TWD"
    TZS = "TZS"
    UAH = "UAH"
    UAK = "UAK"
    UGS = "UGS"
    UGW = "UGW"
    UGX = "UGX"
    USD = "USD"
    USN = "USN"
    USS = "USS"
    UYI = "UYI"
    UYN = "UYN"
    UYP = "UYP"
    UYU = "UYU"
    UYW = "UYW"
    UZS = "UZS"
    VEB = "VEB"
    VEF = "VEF"
    VES = "VES"
    VNC = "VNC"
    VND = "VND"
    VUV = "VUV"
    WST = "WST"
    XAF = "XAF"
    XAG = "XAG"
    XAU = "XAU"
    XBA = "XBA"
    XBB = "XBB"
    XBC = "XBC"
    XBD = "XBD"
    XCD = "XCD"
    XDR = "XDR"
    XEU = "XEU"
    XFO = "XFO"
    XFU = "XFU"
    XOF = "XOF"
    XPD = "XPD"
    XPF = "XPF"
    XPT = "XPT"
    XRE = "XRE"
    XSU = "XSU"
    XTS = "XTS"
    XUA = "XUA"
    XXX = "XXX"
    YDD = "YDD"
    YER = "YER"
    YUD = "YUD"
    YUM = "YUM"
    YUN = "YUN"
    ZAL = "ZAL"
    ZAR = "ZAR"
    ZMK = "ZMK"
    ZMW = "ZMW"
    ZRN = "ZRN"
    ZRZ = "ZRZ"
    ZWC = "ZWC"
    ZWD = "ZWD"
    ZWL = "ZWL"
    ZWN = "ZWN"
    ZWR = "ZWR"


class ProcurementMethod(EnumTemplate):
    open = "open"
    selective = "selective"
    limited = "limited"
    direct = "direct"
    # NoneType_None = None


class MainProcurementCategory(EnumTemplate):
    goods = "goods"
    works = "works"
    services = "services"
    # NoneType_None = None


class AwardStatus(EnumTemplate):
    pending = "pending"
    active = "active"
    cancelled = "cancelled"
    unsuccessful = "unsuccessful"


class ContractStatus(EnumTemplate):
    pending = "pending"
    active = "active"
    cancelled = "cancelled"
    terminated = "terminated"


class MilestoneStatus(EnumTemplate):
    scheduled = "scheduled"
    met = "met"
    notMet = "notMet"
    partiallyMet = "partiallyMet"


class Document(DocumentTemplate):
    """
    Attributes
    ----------
    documentType : str
        A classification of the document described, using the open documentType codelist.
    title : str
        The document title.
    description : str
        A short description of the document. Descriptions are recommended to not exceed 250 words. In the event the document is not accessible online, the description field can be used to describe arrangements for obtaining a copy of the document.
    url : str
        A direct link to the document or attachment. The server providing access to this document ought to be configured to correctly report the document mime type.
    datePublished : dateTime
        The date on which the document was first published. This is particularly important for legally important documents such as notices of a tender.
    dateModified : dateTime
        The date on which the document was modified.
    format : str
        The format of the document, using the open IANA Media Types codelist (see the values in the 'Template' column), or using the 'offline/print' code if the described document is published offline. For example, web pages have a format of 'text/html'.
    language : str
        The language of the linked document using either two-letter ISO639-1, or extended BCP47 language tags. The use of lowercase two-letter codes from ISO639-1 is recommended unless there is a clear user need for distinguishing the language subtype.
    """

    _key = RandomKey()
    documentType: Optional[str]
    title: Optional[str]
    description: Optional[str]
    url: Optional[
        str
    ]  # For now as str, it is possible to change type to url in dashboard. TODO: change it here
    datePublished: Optional[datetime]
    dateModified: Optional[datetime]
    format: Optional[str]
    language: Optional[str]


class Change(DocumentTemplate):
    """
    Attributes
    ----------
    property : str
        The property name that has been changed rel:wative to the place the amendment is. For example if the contract value has changed, then the property under changes within the contract.amendment would be value.amount. (Deprecated in 1.1)
    former_value : str
        The previous value of the changed property, in whatever type the property is. (Deprecated in 1.1)
    """

    property: Optional[str]
    former_value: Optional[Union[str, float, int, List[Any], Dict[str, Any]]]


class Amendment(DocumentTemplate):
    """
    Attributes
    ----------
    date : datetime
        The date of this amendment
    rationale : str
        An explanation for the amendment
    id : str
        An identifier for this amendment: often the amendment number
    description : str
        A free text, or semi-structured, description of the changes made in this amendment.
    amendsReleaseID : str
        Provide the identifier (release.id) of the OCDS release (from this contracting process) that provides the values for this contracting process **before** the amendment was made.
    releaseID : str
        Provide the identifier (release.id) of the OCDS release (from this contracting process) that provides the values for this contracting process **after** the amendment was made.
    changes : List[Change]
        An array of change objects describing the fields changed, and their former values. (Deprecated in 1.1)
    """

    date: Optional[datetime]
    rationale: Optional[str]
    id: Optional[str]
    description: Optional[str]
    amendsReleaseID: Optional[str]
    releaseID: Optional[str]
    changes: List[Change]


class Classification(DocumentTemplate):
    """
    Attributes
    ----------
    scheme : str
        The scheme or codelist from which the classification code is taken. For line item classifications, this uses the open Organization Identifier Scheme codelist.
    id : Union[str, int]
        The classification code taken from the scheme.
    description : str
        A textual description or title for the classification code.
    uri : str
        A URI to uniquely identify the classification code.
    """

    scheme: Optional[str]
    id: Optional[Union[str, int]]
    description: Optional[str]
    uri: Optional[str]


class Identifier(DocumentTemplate):
    """
    Attributes
    ----------
    scheme : str
        Organization identifiers should be taken from an existing organization identifier list. The scheme field is used to indicate the list or register from which the identifier is taken. This value should be taken from the Organization Identifier Scheme codelist.
    id : Union[str, int]
        The identifier of the organization in the selected scheme.
    legalName : str
        The legally registered name of the organization.
    uri : str
        A URI to identify the organization, such as those provided by Open Corporates or some other relevant URI provider. This is not for listing the website of the organization: that can be done through the URL field of the Organization contact point.',
    """

    scheme: Optional[str]
    id: Optional[Union[str, int]]
    legalName: Optional[str]
    uri: Optional[str]


class ContactPoint(DocumentTemplate):
    """
    Attributes
    ----------
    name : str
        The name of the contact person, department, or contact point, for correspondence relating to this contracting process.
    email : str
        The e-mail address of the contact point/person.
    telephone : str
        The telephone number of the contact point/person. This should include the international dialing code.
    faxNumber : str
        The fax number of the contact point/person. This should include the international dialing code.
    url : str
        A web address for the contact point/person.
    """

    name: Optional[str]
    email: Optional[str]
    telephone: Optional[str]
    faxNumber: Optional[str]
    url: Optional[str]


class Value(DocumentTemplate):
    """
    Attributes
    ----------
    amount : float
        Amount as a number.
    currency : Currency
      The currency of the amount, from the closed currency codelist.

    """

    amount: Optional[float]
    currency: Optional[Currency]


class Period(DocumentTemplate):
    """
    Attributes
    ----------
    startDate : datetime
        The start date for the period. When known, a precise start date must be provided.
    endDate : datetime
        The end date for the period. When known, a precise end date must be provided.
    maxExtentDate : datetime
        The period cannot be extended beyond this date. This field can be used to express the maximum available date for extension or renewal of this period.
    durationInDays : int
        The maximum duration of this period in days. A user interface can collect or display this data in months or years as appropriate, and then convert it into days when storing this field. This field can be used when exact dates are not known. If a startDate and endDate are set, this field, if used, should be equal to the difference between startDate and endDate. Otherwise, if a startDate and maxExtentDate are set, this field, if used, should be equal to the difference between startDate and maxExtentDate.
    """

    startDate: Optional[datetime]
    endDate: Optional[datetime]
    maxExtentDate: Optional[datetime]
    durationInDays: Optional[int]


class RelatedProcess(DocumentTemplate):
    """
    Attributes
    ----------
    id : str
        A local identifier for this relationship, unique within this array.
    relationship : str
        The type of relationship, using the open relatedProcess codelist.
    title : str
        The title of the related process, where referencing an open contracting process, this field should match the tender/title field in the related process.
    scheme : str
        The identification scheme used by this cross-reference, using the open relatedProcessScheme codelist.
    identifier : str
        The identifier of the related process. If the scheme is 'ocid', this must be an Open Contracting ID (ocid).
    uri : str
        A URI pointing to a machine-readable document, release or record package containing the identified related process.
    """

    id: Optional[str]
    # changed from list TODO: check if there is the way to change it back
    relationship: Set[str]
    title: Optional[str]
    scheme: Optional[str]
    identifier: Optional[str]
    uri: Optional[str]


class Milestone(DocumentTemplate):
    """
    Attributes
    ----------
    id : Union[constr(min_length=1), int]
        A local identifier for this milestone, unique within this block. This field is used to keep track of multiple revisions of a milestone through the compilation from release to record mechanism.',
    title : Optional[str]
        Milestone title
    type : Optional[str]
        The nature of the milestone, using the open milestoneType codelist.
    description : Optional[str]
        A description of the milestone.
    code : Optional[str]
        Milestone codes can be used to track specific events that take place for a particular kind of contracting process. For example, a code of 'approvalLetter' can be used to allow applications to understand this milestone represents the date an approvalLetter is due or signed.
    dueDate : Optional[datetime]
        The date the milestone is due.
    dateMet : Optional[datetime]
        The date on which the milestone was met.
    dateModified : Optional[datetime]
        The date the milestone was last reviewed or modified and the status was altered or confirmed to still be correct.
    status : Optional[MilestoneStatus]
        description='The status that was realized on the date provided in `dateModified`, from the closed milestoneStatus codelist.',
    documents : Optional[Document]
        List of documents associated with this milestone (Deprecated in 1.1).
    """

    # min_lenght check how to make that constraint
    id: Optional[Union[str, int]]
    title: Optional[str]
    type: Optional[str]
    description: Optional[str]
    code: Optional[str]
    dueDate: Optional[datetime]
    dateMet: Optional[datetime]
    dateModified: Optional[datetime]
    status: Optional[MilestoneStatus]
    documents: Optional[Document]


class Budget(DocumentTemplate):
    """
    Attributes
    ----------
    id : Optional[Union[str, int]]
        An identifier for the budget line item which provides funds for this contracting process. This identifier should be possible to cross-reference against the provided data source.
    description : Optional[str]
        A short free text description of the budget source. May be used to provide the title of the budget line, or the programme used to fund this project.
    amount : Optional[Value]
        The value reserved in the budget for this contracting process. A negative value indicates anticipated income to the budget as a result of this contracting process, rather than expenditure. Where the budget is drawn from multiple sources, the budget breakdown extension can be used.
    project : Optional[str]
        The name of the project through which this contracting process is funded (if applicable). Some organizations maintain a registry of projects, and the data should use the name by which the project is known in that registry. No translation option is offered for this string, as translated values can be provided in third-party data, linked from the data source above.
    projectID : Optional[Union[str, int]]
        An external identifier for the project that this contracting process forms part of, or is funded via (if applicable). Some organizations maintain a registry of projects, and the data should use the identifier from the relevant registry of projects.
    uri : Optional[str]
        A URI pointing directly to a machine-readable record about the budget line-item or line-items that fund this contracting process. Information can be provided in a range of formats, including using IATI, the Open Fiscal Data Standard or any other standard which provides structured data on budget sources. Human readable documents can be included using the planning.documents block.
    source : Optional[str]
        (Deprecated in 1.1) Used to point either to a corresponding Budget Data Package, or to a machine or human-readable source where users can find further information on the budget line item identifiers, or project identifiers, provided here.
    """

    id: Optional[Union[str, int]]
    description: Optional[str]
    amount: Optional[Value]
    project: Optional[str]
    projectID: Optional[Union[str, int]]
    uri: Optional[str]
    source: Optional[str]


class Organization(DocumentTemplate):
    """
    Attributes
    ----------
    name : Optional[str]
        A common name for this organization or other participant in the contracting process. The identifier object provides a space for the formal legal name, and so this may either repeat that value, or may provide the common name by which this organization or entity is known. This field may also include details of the department or sub-unit involved in this contracting process.
    id : Optional[str]
        The ID used for cross-referencing to this party from other sections of the release. This field may be built with the following structure {identifier.scheme}-{identifier.id}(-{department-identifier}).
    identifier : Optional[Identifier]
        The primary identifier for this organization or participant. Identifiers that uniquely pick out a legal entity should be preferred. Consult the organization identifier guidance for the preferred scheme and identifier to use.
    additionalIdentifiers : Optional[Identifier]
        A list of additional / supplemental identifiers for the organization or participant, using the organization identifier guidance. This can be used to provide an internally used identifier for this organization in addition to the primary legal entity identifier.
    address : Optional[Address]
        An address. This may be the legally registered address of the organization, or may be a correspondence address for this particular contracting process.
    contactPoint : Optional[ContactPoint]
        Contact details that can be used for this party.',
    roles : Optional[str]
        The party's role(s) in the contracting process, using the open partyRole codelist.
    details : Dict
        Additional classification information about parties can be provided using partyDetail extensions that define particular fields and classification schemes.
    """

    name: Optional[str]
    id: Optional[str]
    identifier: Optional[Identifier]
    additionalIdentifiers: Set[Identifier]
    address: Optional[Address]
    contactPoint: Optional[ContactPoint]
    roles: Set[str]
    details: Set[str]


class Unit(DocumentTemplate):
    """
    Attributes
    ----------
    scheme : Optional[str]
        description="The list from which identifiers for units of measure are taken, using the open unitClassificationScheme codelist.
    id : Optional[str]
        The identifier from the codelist referenced in the `scheme` field. Check the unitClassificationScheme codelist for details of how to find and use identifiers from the scheme in use.
    name : Optional[str]
        Name of the unit.
    value : Optional[Value]
        The monetary value of a single unit.
    uri : Optional[str]
        The machine-readable URI for the unit of measure, provided by the scheme.
    """

    scheme: Optional[str]
    id: Optional[str]
    name: Optional[str]
    value: Optional[Value]
    uri: Optional[str]


class Item(DocumentTemplate):
    """
    Attributes
    ----------
    id : Union[str, int]
        A local identifier to reference and merge the items by. Must be unique within a given array of items.
    description : Optional[str]
        A description of the goods, services to be provided.
    classification : Optional[Classification]
        The primary classification for the item.
    additionalClassifications : Optional[Classification]
        An array of additional classifications for the item.
    quantity : Optional[float]
        The number of units to be provided.
    unit : Optional[Unit]
        A description of the unit in which the supplies, services or works are provided (e.g. hours, kilograms) and the unit-price.
    """

    # TODO: min_length=1 constraint
    id: Optional[Union[str, int]]
    description: Optional[str]
    classification: Optional[Classification]
    # TODO: make it a list if needed
    additionalClassifications: Set[Classification]  # Optional[Classification]
    # unique_items=True, TODO: check how to make this constraint in TerminusDB syntax
    quantity: Optional[float]
    unit: Optional[Unit]


class Planning(DocumentTemplate):
    """
    Attributes
    ----------
    rationale : Optional[str]
        The rationale for the procurement provided in free text. More detail can be provided in an attached document.
    budget : Optional[Budget]
        Details of the budget that funds this contracting process.
    documents : Optional[Document]
        A list of documents related to the planning process.
    milestones : Optional[Milestone]
        A list of milestones associated with the planning stage.
    """

    rationale: Optional[str]
    budget: Optional[Budget]
    # TODO: make it a list if needed
    documents: Optional[Document]
    # TODO: make it a list if needed
    milestones: Set[Milestone]


class Tender(DocumentTemplate):
    """
    Attributes
    ----------
    id : Union[str, int]
        An identifier for this tender process. This may be the same as the ocid, or may be an internal identifier for this tender.
    title : Optional[str]
        A title for this tender. This will often be used by applications as a headline to attract interest, and to help analysts understand the nature of this procurement.
    description : Optional[str]
        A summary description of the tender. This complements any structured information provided using the items array. Descriptions should be short and easy to read. Avoid using ALL CAPS.',
    status : Optional[Status]
        The current status of the tender, from the closed tenderStatus codelist.
    procuringEntity : Optional[Organization]
        The entity managing the procurement. This may be different from the buyer who pays for, or uses, the items being procured.
    items : Optional[Item]
        The goods and services to be purchased, broken into line items wherever possible. Items should not be duplicated, but the quantity specified instead.
    value : Optional[Value]
        The total upper estimated value of the procurement. A negative value indicates that the contracting process may involve payments from the supplier to the buyer (commonly used in concession contracts).
    minValue : Optional[Value]
        The minimum estimated value of the procurement.  A negative value indicates that the contracting process may involve payments from the supplier to the buyer (commonly used in concession contracts).
    procurementMethod : Optional[ProcurementMethod]
        The procurement method, from the closed method codelist.
    procurementMethodDetails : Optional[str]
        Additional detail on the procurement method used. This field can be used to provide the local name of the particular procurement method used.
    procurementMethodRationale : Optional[str]
        Rationale for the chosen procurement method. This is especially important to provide a justification in the case of limited tenders or direct awards.
    mainProcurementCategory : Optional[MainProcurementCategory]
        The primary category describing the main object of this contracting process, from the closed procurementCategory codelist.
    additionalProcurementCategories : Optional[str]
        Any additional categories describing the objects of this contracting process, using the open extendedProcurementCategory codelist.
    awardCriteria : Optional[str]
        The award criteria for the procurement, using the open awardCriteria codelist.
    awardCriteriaDetails : Optional[str]
        Any detailed or further information on the award or selection criteria.
    submissionMethod : Optional[str]
        The methods by which bids are submitted, using the open submissionMethod codelist.
    submissionMethodDetails : Optional[str]
        Any detailed or further information on the submission method. This can include the address, e-mail address or online service to which bids are submitted, and any special requirements to be followed for submissions.
    tenderPeriod : Optional[Period]
        The period when the tender is open for submissions. The end date is the closing date for tender submissions.
    enquiryPeriod : Optional[Period]
        The period during which potential bidders may submit questions and requests for clarification to the entity managing procurement. Details of how to submit enquiries should be provided in attached notices, or in submissionMethodDetails. Structured dates for when responses to questions will be made can be provided using tender milestones.
    hasEnquiries : Optional[bool]
        A true/false field to indicate whether any enquiries were received during the tender process. Structured information on enquiries that were received, and responses to them, can be provided using the enquiries extension.
    eligibilityCriteria : Optional[str]
        A description of any eligibility criteria for potential suppliers.
    awardPeriod : Optional[Period]
        The period for decision making regarding the contract award. The end date should be the date on which an award decision is due to be finalized. The start date may be used to indicate the start of an evaluation period.
    contractPeriod : Optional[Period]
        The period over which the contract is estimated or required to be active. If the tender does not specify explicit dates, the duration field may be used.
    numberOfTenderers : Optional[int]
        The number of parties who submit a bid.
    tenderers : Optional[Organization]
        All parties who submit a bid on a tender. More detailed information on bids and the bidding organization can be provided using the bid extension.
    documents : Optional[Document]
        All documents and attachments related to the tender, including any notices. See the documentType codelist for details of potential documents to include. Common documents include official legal notices of tender, technical specifications, evaluation criteria, and, as a tender process progresses, clarifications and replies to queries.
    milestones : Optional[Milestone]
        A list of milestones associated with the tender.
    amendments : Optional[Amendment]
        A tender amendment is a formal change to the tender, and generally involves the publication of a new tender notice/release. The rationale and a description of the changes made can be provided here.
    amendment : Optional[Amendment]
        The use of individual amendment objects has been deprecated. From OCDS 1.1 information should be provided in the amendments array.
    crossBorderLaw : Optional[str]
        Description
    """

    # TODO: implement min_length=1 constraint
    # id: Union[str, int]
    id: Optional[Union[str, int]]
    title: Optional[str]
    description: Optional[str]
    status: Optional[Status]
    procuringEntity: Optional[Organization]
    # make it list of Items
    items: Set[Item]  # Optional[Item]
    # unique_items=True, TODO: find out how to implement in TerminusDB syntax
    value: Optional[Value]
    minValue: Optional[Value]
    procurementMethod: Optional[ProcurementMethod]
    procurementMethodDetails: Optional[str]
    procurementMethodRationale: Optional[str]
    mainProcurementCategory: Optional[MainProcurementCategory]
    # TODO: make it list of strings
    additionalProcurementCategories: Set[str]  # Optional[str]
    awardCriteria: Optional[str]
    awardCriteriaDetails: Optional[str]
    # TODO: make it list of strings
    submissionMethod: Set[str]  # Optional[Union[str, List[str], None]]
    submissionMethodDetails: Optional[str]
    tenderPeriod: Optional[Period]
    enquiryPeriod: Optional[Period]
    hasEnquiries: Optional[bool]
    eligibilityCriteria: Optional[str]
    awardPeriod: Optional[Period]
    contractPeriod: Optional[Period]
    numberOfTenderers: Optional[int]
    # TODO: make it list of Organization
    tenderers: Set[Organization]  # Optional[Organization]
    # unique_items=True, TODO: find out how to apply this constraint in TerminusDB syntax
    # TODO: make it list of documents
    documents: Set[Document]  # Optional[Document]
    # TODO: make it list of Milestones
    milestones: Set[Milestone]  # Optional[Milestone]
    # TODO: make it list of Amendments
    amendments: Set[Amendment]  # Optional[Amendment]
    amendment: Optional[Amendment]
    crossBorderLaw: Optional[str]


class Award(DocumentTemplate):
    """
    id : Union[str, int]
        The identifier for this award. It must be unique and must not change within the Open Contracting Process it is part of (defined by a single ocid). See the identifier guidance for further details.'
    title : Optional[str]
        Award title'
    description : Optional[str]
        Award description
    status : Optional[AwardStatus]
        The current status of the award, from the closed awardStatus codelist.
    date : Optional[datetime]
        The date of the contract award. This is usually the date on which a decision to award was made.
    value : Optional[Value]
        The total value of this award. In the case of a framework contract this may be the total estimated lifetime value, or maximum value, of the agreement. There may be more than one award per procurement. A negative value indicates that the award may involve payments from the supplier to the buyer (commonly used in concession contracts).
    suppliers : List[Organization]
        The suppliers awarded this award. If different suppliers have been awarded different items or values, these should be split into separate award blocks.
    items : Optional[Item]
        The goods and services awarded in this award, broken into line items wherever possible. Items should not be duplicated, but the quantity specified instead.
    contractPeriod : Optional[Period]
        The period for which the contract has been awarded.
    documents : Optional[Document]
        All documents and attachments related to the award, including any notices.
    amendments : Optional[Amendment]
        An award amendment is a formal change to the details of the award, and generally involves the publication of a new award notice/release. The rationale and a description of the changes made can be provided here.
    amendment : Optional[Amendment]
        The use of individual amendment objects has been deprecated. From OCDS 1.1 information should be provided in the amendments array.
    """

    # TODO: implement min_length=1 constraint
    id: Optional[Union[str, int]]
    title: Optional[str]
    description: Optional[str]
    status: Optional[AwardStatus]
    date: Optional[datetime]
    value: Optional[Value]
    # TODO: make it list and implement unique_items=True
    suppliers: Set[Organization]  # Optional[Union[str, List[Organization], None]]
    # TODO: make it list and implement unique_items=True, and min_items=1
    items: Set[Item]  # Optional[Union[str, List[Item], None]]
    contractPeriod: Optional[Period]
    # TODO: make it list and implement unique_items=True
    documents: Set[Document]  # Optional[Union[str, List[Document], None]]
    # TODO: make it list
    amendments: Set[Amendment]  # Optional[Union[str, List[Amendment], None]]
    amendment: Optional[Amendment]


class Transaction(DocumentTemplate):
    """
    Attributes
    ----------
    id : Union[str, int]
        A unique identifier for this transaction. This identifier should be possible to cross-reference against the provided data source. For IATI this is the transaction reference.
    source : Optional[str]
        Used to point either to a corresponding Fiscal Data Package, IATI file, or machine or human-readable source where users can find further information on the budget line item identifiers, or project identifiers, provided here.
    date : Optional[datetime]
        The date of the transaction.
    value : Optional[Value]
        The value of the transaction.
    payer : Optional[Organization]
        An organization reference for the organization from which the funds in this transaction originate.
    payee : Optional[Organization]
        An organization reference for the organization which receives the funds in this transaction.
    uri : Optional[str]
        A URI pointing directly to a machine-readable record about this spending transaction.
    amount : Optional[Value]
        (Deprecated in 1.1. Use transaction.value instead) The value of the transaction. A negative value indicates a refund or correction.
    providerOrganization : Optional[Identifier]
        (Deprecated in 1.1. Use transaction.payer instead.) The Organization Identifier for the organization from which the funds in this transaction originate. Expressed following the Organizational Identifier standard - consult the documentation and the codelist.
    receiverOrganization : Optional[Identifier]
        (Deprecated in 1.1. Use transaction.payee instead). The Organization Identifier for the organization which receives the funds in this transaction. Expressed following the Organizational Identifier standard - consult the documentation and the codelist.
    """

    # TODO: implement min_length constraint for string part of the union
    id: Optional[Union[str, int]]
    source: Optional[str]
    date: Optional[datetime]
    value: Optional[Value]
    payer: Optional[Organization]
    payee: Optional[Organization]
    uri: Optional[str]
    amount: Optional[Value]
    providerOrganization: Optional[Identifier]
    receiverOrganization: Optional[Identifier]


# TODO: make all 3 attributes a list and implement unique_items=true constraint
class Implementation(DocumentTemplate):
    """
    Attributes
    ----------
    transactions : Optional[Transaction]
        A list of the spending transactions made against this contract
    milestones : Optional[Milestone]
        As milestones are completed, the milestone's status and dates should be updated.
    documents : Optional[Document]
        Documents and reports that are part of the implementation phase e.g. audit and evaluation reports.
    """

    transactions: Set[Transaction]
    milestones: Set[Milestone]
    documents: Set[Document]


class Contract(DocumentTemplate):
    """
    id : Union[str, int]
        The identifier for this contract. It must be unique and must not change within the Open Contracting Process it is part of (defined by a single ocid). See the identifier guidance for further details.
    awardID : Union[str, int]
        The award.id against which this contract is being issued.
    title : Optional[str]
        Contract title', title='Contract title.
    description : Optional[str]
        Contract description', title='Contract description'
    status : Optional[ContractStatus]
        The current status of the contract, from the closed contractStatus codelist.
    period : Optional[Period]
        The start and end date for the contract.', title='Period'
    value : Optional[Value]
        The total value of this contract. A negative value indicates that the contract will involve payments from the supplier to the buyer (commonly used in concession contracts).
    items : Optional[Item]
        The goods, services, and any intangible outcomes in this contract. Note: If the items are the same as the award do not repeat.
    dateSigned : Optional[datetime]
        The date the contract was signed. In the case of multiple signatures, the date of the last signature.
    documents : Optional[Document]
        All documents and attachments related to the contract, including any notices.
    implementation : Optional[Implementation]
        Information related to the implementation of the contract in accordance with the obligations laid out therein.
    relatedProcesses : Optional[RelatedProcess]
        The details of related processes: for example, if this process is followed by one or more contracting processes, represented under a separate open contracting identifier (ocid). This is commonly used to refer to subcontracts and to renewal or replacement processes for this contract.
    milestones : Optional[Milestone]
        A list of milestones associated with the finalization of this contract.
    amendments : Optional[Amendment]
        A contract amendment is a formal change to, or extension of, a contract, and generally involves the publication of a new contract notice/release, or some other documents detailing the change. The rationale and a description of the changes made can be provided here.
    amendment : Optional[Amendment]
        The use of individual amendment objects has been deprecated. From OCDS 1.1 information should be provided in the amendments array.
    """

    # TODO: implement min_length constraint for string part of the union
    id: Optional[Union[str, int]]
    awardID: Union[str, int]
    title: Optional[str]
    description: Optional[str]
    status: Optional[ContractStatus]
    period: Optional[Period]
    value: Optional[Value]
    # TODO: make it list and implement unique_items=True, and min_items=1
    items: Set[Item]
    dateSigned: Optional[str]  # Optional[datetime]
    # TODO: make it list and implement unique_items=True
    documents: Set[Document]
    implementation: Optional[Implementation]
    # TODO: make it list and implement unique_items=True
    relatedProcesses: Set[RelatedProcess]
    # TODO: make it list
    milestones: Set[Milestone]
    # TODO: make it list
    amendments: Set[Amendment]
    amendment: Optional[Amendment]


class Release(DocumentTemplate):
    """
    Attributes
    ----------
    ocid : constr(min_length=1)
        A globally unique identifier for this Open Contracting Process. Composed of an ocid prefix and an identifier for the contracting process. For more information see the Open Contracting Identifier guidance.
    id : constr(min_length=1)
        An identifier for this particular release of information. A release identifier must be unique within the scope of its related contracting process (defined by a common ocid). A release identifier must not contain the # character.
    date : datetime
        The date on which the information contained in the release was first recorded in, or published by, any system.
    tag : List[Tag]
        One or more values from the closed releaseTag codelist. Tags can be used to filter releases and to understand the kind of information that releases might contain.
    initiationType : InitiationType
        The type of initiation process used for this contract, from the closed initiationType codelist.
    parties : Optional[Organization]
        Information on the parties (organizations, economic operators and other participants) who are involved in the contracting process and their roles, e.g. buyer, procuring entity, supplier etc. Organization references elsewhere in the schema are used to refer back to this entries in this list.
    buyer : Optional[Organization]
        A buyer is an entity whose budget will be used to pay for goods, works or services related to a contract. This may be different from the procuring entity who may be specified in the tender data.
    planning : Optional[Planning]
        Information from the planning phase of the contracting process. This includes information related to the process of deciding what to contract, when and how.
    tender : Optional[Tender]
        The activities undertaken in order to enter into a contract.
    awards : Optional[Award]
        Information from the award phase of the contracting process. There can be more than one award per contracting process e.g. because the contract is split among different providers, or because it is a standing offer.
    contracts : Optional[Contract]
        Information from the contract creation phase of the procurement process.
    language : Optional[str]
        The default language of the data using either two-letter ISO639-1 or extended BCP47 language tags. The use of lowercase two-letter codes from ISO639-1 is recommended.
    relatedProcesses : Optional[RelatedProcess]
        The details of related processes: for example, if this process follows on from one or more other processes, represented under a separate open contracting identifier (ocid). This is commonly used to relate mini-competitions to their parent frameworks or individual tenders to a broader planning process.
    """

    # TODO: implement min_length constraint
    ocid: str
    # TODO: implement min_length constraint
    id: Optional[str]
    date: datetime
    # TODO: implement min_items=1 constraint
    tag: List[Tag]
    initiationType: InitiationType
    # TODO: implement unique_items constraint and make it a list
    parties: Set[Organization]  # Optional[Union[str, List[Organization], None]]
    buyer: Set[Organization]
    planning: Optional[Planning]
    tender: Optional[Tender]
    # TODO: implement unique_items constraint and make it a list
    awards: Set[Award]  # Optional[Union[str, List[Award], None]]
    # TODO: implement unique_items constraint and make it a list
    contracts: Set[Contract]  # Optional[Union[str, List[Contract], None]]
    language: Optional[str]
    # TODO: implement unique_items constraint and make it a list
    relatedProcesses: Set[RelatedProcess]  # Optional[Union[str, RelatedProcess, None]]
