# SPDX-FileCopyrightText: 2023 Free Software Foundation Europe <contact@fsfe.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later
import configparser
import enum
import os

from terminusdb_client import Client, woqlschema
from terminusdb_client.woqlschema import WOQLSchema

from tedective_api import schema


def get_connection():
    """Returns a connection to the database."""
    team = os.environ["DB_TEAM"]
    user = os.environ["DB_USER"]
    db = os.environ["DB_NAME"]
    key = os.environ["DB_KEY"]

    address = os.environ["DB_ADDRESS"]

    client = Client(address)
    client.connect(team=team, db=db, user=user, key=key)
    return client


def init_db():
    """Initialization of the database"""
    team = os.environ["DB_TEAM"]
    key = os.environ["DB_KEY"]
    user = os.environ["DB_USER"]
    db = os.environ["DB_NAME"]
    address = os.environ["DB_ADDRESS"]

    client = Client(address)
    client.connect(key=key, team=team, user=user)
    client.create_database(db, team, db, "")
    upload_schema(client)


def cleanup_testing_db():
    """Deletes the testing database"""
    team = os.environ["TESTING_DB_TEAM"]
    key = os.environ["TESTING_DB_KEY"]
    user = os.environ["TESTING_DB_USER"]
    db = os.environ["TESTING_DB_NAME"]
    address = os.environ["TESTING_DB_ADDRESS"]

    client = Client(address)

    client.connect(key=key, team=team, user=user)
    client.delete_database(team=team, dbid=db)


def prepare_testing_db():
    """Initialize database for testing"""
    team = os.environ["TESTING_DB_TEAM"]
    key = os.environ["TESTING_DB_KEY"]
    user = os.environ["TESTING_DB_USER"]
    db = os.environ["TESTING_DB_NAME"]
    address = os.environ["TESTING_DB_ADDRESS"]

    client = Client(address)
    client.connect(key=key, team=team, user=user)
    client.create_database(db, team, db, "")
    upload_schema(client)
    return client


def upload_schema(client):
    """Commit the schema, used fo database initialization"""
    # SPDX-SnippetBegin
    # SPDX-License-Identifier: Apache-2.0
    # SPDX-SnippetCopyrightText: 2021-23 TerminusDB Team <team@terminusdb.com>
    schema_plan = schema
    last_item = None
    documentation = {}
    if schema_plan.__doc__:
        for line in schema_plan.__doc__.split("\n"):
            if "Title:" in line:
                documentation["title"] = line[6:].strip()
                last_tiem = documentation["title"]
            elif "Description:" in line:
                documentation["description"] = line[12:].strip()
                last_tiem = documentation["description"]
            elif "Authors:" in line:
                documentation["authors"] = line[8:].strip()
                last_tiem = documentation["authors"]
            elif last_item is not None:
                last_item += "\n" + line.strip()
    authors = documentation.get("authors")
    if authors:
        authors = documentation["authors"].split(",")
        authors = [x.strip() for x in authors]
    schema_obj = WOQLSchema(
        title=documentation.get("title"),
        description=documentation.get("description"),
        authors=authors,
    )
    for obj_str in dir(schema_plan):
        obj = eval(f"schema_plan.{obj_str}")
        if isinstance(obj, woqlschema.TerminusClass) or isinstance(obj, enum.EnumMeta):
            if obj_str not in ["DocumentTemplate", "EnumTemplate", "TaggedUnion"]:
                schema_obj.add_obj(obj.__name__, obj)
    schema_obj.commit(client, full_replace=True)
    # SPDX-SnippetEnd


if __name__ == "__main__":
    init_db()
