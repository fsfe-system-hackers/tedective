<!--
SPDX-FileCopyrightText: 2022 Free Software Foundation Europe <contact@fsfe.org>

SPDX-License-Identifier: CC-BY-SA-4.0
-->

[![Build Status](https://drone.fsfe.org/api/badges/TEDective/tedective/status.svg)](https://drone.fsfe.org/TEDective/tedective)
[![REUSE status](https://api.reuse.software/badge/git.fsfe.org/TEDective/tedective)](https://api.reuse.software/info/git.fsfe.org/TEDective/tedective)
[![Black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)

**Please note:** This project is in an alpha stage and many things are subject to
change. But we are getting there. Expect a workable beta-stage prototype towards the end
of Q2 2023.

![TEDective Logo](https://git.fsfe.org/TEDective/tedective/raw/branch/main/logo.png)

# What is TEDective?

**tl;dr** TEDective helps you find out who buys what from whom in the EU.

A related aim is to bring is to bring the [XML files published by the European
Union](https://ted.europa.eu) into shape by transforming them to the [Open
Contracting Data Standard](https://standard.open-contracting.org/latest/en/).

Despite a [range of previous efforts](#previous-efforts) to parse and analyse
TED data, there currently exists no offering that fulfils _all_ of the following
requirements with regards to the provision of TED data:

1. It is built and published under a free software license.
2. It offers a current, cleaned and deduplicated version of TED data.
3. Data is available as OCDS-compliant JSON, both as bulk downloads and via a
   capable and well-documented API.

Sustainably providing long-term access to European tender data in a way that fulfils
these three requirements enables numerous applications that might be of interest to
civil society, business and government which could greatly enhance the transparency and
accountability of European business activity. There are a range of interesting questions
that can be answered with this data if it was available in a well-documented and
easy-to-understand format that is interoperable with tender data published elsewhere.

## What's inside

TED XML notices are downloaded and parsed
into [PostgreSQL](https://www.postgresql.org/) (and possibly later a graph
database) using the amazing [SQLModel](https://sqlmodel.tiangolo.com/). An API built
with [FastAPI](https://fastapi.tiangolo.com/) sits in front of this database and
provides streamlined access to OCDS entities, such as organizations, releases or
contracts. The code that does the parsing and serves the API can be found in
the [`./api`](./api) folder.

The second component of this monorepo is the React-based UI, built
with [NextJS](https://nextjs.org/), [Chakra UI](https://chakra-ui.com/) and
[react-force-graph](https://vasturiano.github.io/react-force-graph/). All the 
relevant code for this component lives in the [`./ui`](./ui) folder.

Finally, there is the project landing page and documentation built
with [Docusaurus](https://docusaurus.io/). The relevant code lives
in the [`./website`](./website) folder.

## Development

To start developing locally, please consult the
[documentation](https://tedective.org/docs/quickstart) or simply run:

``` shell
make help
```

## Deployment

The production deployment is handled via the [`.drone.yml`](./.drone.yml) which 
simply executes the relevant `docker-compose` files, which currently are:

- [`./docker-compose.production.yml`](./docker-compose.production.yml) for the 
  production deployment
- [`./api/docker-compose.test.yml`](./api/docker-compose.test.yml) for the automated tests of
  API and database
- [`./docker-compose.tdb.yml`](./docker-compose.tdb.yml) for the
  TerminusDB deployment on the (temporarily, will be merged into `./docker-compose.production.yml` in the future)

Apart from those, the API and database may be deployed locally with help of relevant `docker-compose` files:
- [`./api/docker-compose.yml`](./api/docker-compose.yml) for the local deployment of TerminusDB and TEDective API
- [`./api/docker-compose-localapi.yml`](./api/docker-compose.localapi.yml) for deployment of TEDective API connected to [remote database](https://tdb.tedective.org/) (useful only for development team)

## Resources

There are a lot of resources that are helpful in developing and extending
the parser. The most important ones are:

1. [The OCDS Schema
   reference](https://standard.open-contracting.org/latest/en/schema/reference/)
   which describes in detail what an OCDS release is and what it should look like
2. [The OCDS European Union
   Profile](https://standard.open-contracting.org/profiles/eu/latest/en/) which
   maps fields in TED notices to fields in an OCDS release
   ([Github](https://github.com/open-contracting-extensions/european-union),
   [Internal
   Tooling](https://github.com/open-contracting/european-union-support/)).
3. [The OJS' Standard form for public
   procurement site](https://simap.ted.europa.eu/web/simap/standard-forms-for-public-procurement)
   lists all the forms used in any given TED notice.

## Previous efforts

- [TheyBuyForYou](https://theybuyforyou.eu) (a project by "a consortium of 10
  leading companies, universities, research centres, government departments and
  local authorities in the UK, Norway, Italy, Spain and Slovenia" funded by the
  [EU Horizon 2020](https://cordis.europa.eu/project/id/780247) programme. The
  project cost the EU around €3.3 million and was developed over two years until
  December 2020. It is now largely dysfunctional and out-of-date. Some code
  seems to be [publicly available](https://github.com/TBFY) but is provided
  without an explicit license)

- [DigiWhist's](https://github.com/digiwhist/master)
  [opentender.eu](https://opentender.eu/start) (seems somewhat abandoned, repo
  is still lightly maintained. Data is updated less than once a month and the
  frontend code is not open-source. One of the DigiWhist researchers foudned
  [TenderX](https://tenderx.eu/#about), a private for-profit tender/company data
  offering)

  **NB**: This dataset [seems to be
  used](https://kingfisher-collect.readthedocs.io/en/latest/_modules/kingfisher_scrapy/spiders/europe_ted_digiwhist.html#EuropeTEDDigiwhist)
  by the OCDS tool for scraping globally available OCDS data releases.

  **NB**: There was apparently
  [some](https://okfn.de/blog/2016/03/public-procurement-digital-era/)
  [collboration](https://okfn.de/blog/2016/07/from-publication-to-award/)
  [between](https://okfn.de/blog/2016/06/who-has-won-the-contract/) the OKFN and
  the lead researcher now behind TenderX (Mihály Fazekas,
  [here](http://mihalyfazekas.eu/) is his personal website).

- [OpenTED](https://github.com/opented) (seems abandoned, last commit 2015;
  didn't work with OCDS as it wasn't developed at the time)
- [opented](https://github.com/datasets/opented) (very old attempt at parsing
  TED data that didn't turn out to work)
- [OpenTED Browser](https://arxiv.org/pdf/1611.02118.pdf) (an academic paper
  about )
- [ExtracTED](https://github.com/ONSBigData/ExtracTED) (according to the README,
  this was used to parse data between 2014-2016; last commit 5 years ago)
- [eu-hack](https://github.com/amansingh9097/eu-hack) (last commit 15 months
  ago, author is a data scientist at Amazon and target format is CSV, I could
  not run his code and achieve an error-free parsing of more recent TED data)
