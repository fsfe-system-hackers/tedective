---
slug: tedective-at-fosdem-2023
title: TEDective at FOSDEM 2023
authors: [linus, alex]
tags: [fosdem, eu]
---

<!--
SPDX-FileCopyrightText: 2022 Free Software Foundation Europe <contact@fsfe.org>

SPDX-License-Identifier: CC-BY-SA-4.0
-->

This is just a quick update: TEDective will be at this year's FOSDEM
hoping to attract the attention of everyone who already is or will become
interested in opening up public procurement data.

Find us in the [Graph systems and algorithms
devroom](https://fosdem.org/2023/schedule/track/graph_systems_and_algorithms/)
on **Saturday, 4 February at 10:30** in room K.4.601.
