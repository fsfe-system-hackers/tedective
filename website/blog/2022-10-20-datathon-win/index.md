---
slug: fsfe-wins-eu-datathon-with-tedective
title: FSFE wins the transparency challenge of the EU Datathon 2022
authors: [linus, alex]
tags: [datathon, eu]
---

<!--
SPDX-FileCopyrightText: 2022 Free Software Foundation Europe <contact@fsfe.org>

SPDX-License-Identifier: CC-BY-SA-4.0
-->

The sixth edition of EU Datathon, the EU’s open data competition, came to a
close last week with the awards ceremony. The Free Software Foundation Europe
(FSFE) won the first prize in the challenge ‘transparency in public procurement’
with a program that helps analyse how public administrations in the European
Union spend their money.

![FSFE wins the EU Datathon](./alex-and-linus.png)

At the EU Datathon finals, that took place in Brussels as part of the [European
Year of Youth](https://youth.europa.eu/year-of-youth_en), TEDective, the project
submitted by the FSFE, ended up winning one of the four proposed challenges,
that Europe is currently facing: the achievement of transparency in public
procurement.

Using open data this Free Software program empowers citizens by making EU
tendering data accesible to everyone who wants to consult and use it. For
example, it will allow a journalist to find out how much money the government
spends on Microsoft licenses and products, but also to compare that spending
with other regions in similar cases or even in comparison with other countries.

“Although it might seem boring at first sight, TED data reveals crucial
information about the economic activity of business and state organisations
alike. As, there was no Free Software solution making this data accessible to
non-experts, this is what we’re trying to do with TEDective”, explains Linus
Sehn, system administrator at FSFE and one of the members of the TEDective team.
This first prize, which comes with a cash reward of 25,000 euros, willl
contribute to raise awareness to the need of making tendering data accesible and
easy to analyze.

### Available for using, understanding sharing and improving

Developed with the help of [Michael Weimann](https://weimann.digital/), and
released as a [REUSE-compliant](https://reuse.software/) project under a Free
Software (also known as Open Source) license, TEDective improves access to the
data published by [Tenders Electronic
Daily](https://ted.europa.eu/TED/browse/browseByMap.do) (TED), fullfilling all
of the following requirements with regards to the provision of TED data: it is
available without costs for commercial as well as non-commercial use; it is
up-to-date (updates at least on a monthly basis), cleaned and both buyers and
suppliers are adequately deduplicated; and it can be downloaded in bulk, making
it available as Open Contracting Data Standard (OCDS) to allow interoperability.
Besides, it will be designed, maintained and monitored transparently and in
close co-operation with all relevant stakeholders and user groups.

Sustainably providing long-term access to European tendering data in a way that
fulfils these requirements could enable numerous applications that are of
interest to civil society, business, the press, and beyond which could greatly
enhance the transparency of business activity in Europe. There are a range of
interesting questions that can be answered with this data if it was available in
a well-documented and easy-to-understand format that is interoperable with
tendering data published elsewhere.

If you want to find out more about TEDective, feel free to check out the [git
repository software](https://git.fsfe.org/TEDective/tedective). The
team is also looking for data experts, who want to help, so if you are
interested, please contact the team by emailing
[tedective@fsfe.org](mailto:tedective@fsfe.org).

Check the [TEDective presentation at the awards
ceremony](https://media.fsfe.org/w/x1buMiq6k7nNyCRj5nxJ52).

### The EU Datathon 2022

Empowering young people in the job market, reducing greenhouse emissions, and
bringing European cultural heritage closer to citizens: these were some of the
ambitions put forward by the 12 finalist teams of this year’s [EU
Datathon](https://op.europa.eu/en/web/eudatathon), the teams were shortlisted
from an initial 156 entries from 38 countries, the highest participation in the
competition’s history, and competed in four categories, all highly relevant to
the challenges Europe faces today: the European Green Deal, transparency in
public procurement, EU public procurement opportunities for young people, and a
Europe fit for the digital age.

Prior to the finals, the finalists had the opportunity to present their ideas
for apps built on EU open data in a [series of
videos](https://www.youtube.com/watch?v=lYiuDfq64-Y&list=PLT5rARDev_rnik8jF6E8k5AjN5zeJmqXG)
while, on the final day, they pitched their polished apps to the jury of 14 open
data experts and the online audience. In his opening speech, Commissioner
Johannes Hahn praised the teams’ innovative approaches underlining that “There
is also a strong positive impact on accountability, transparency, participation,
inclusion and democracy, supporting core European values” while Ms Hilde
Hardeman, Director General of the Publications Office of the EU, highlighted
that “The European Union is well aware of the immense opportunities data offer.
We are truly determined to make the most out of these, for our citizens,
economies, societies”.

The EU Datathon competition is organised annually by the [Publications Office of
the European Union,](https://op.europa.eu/) in support of the [European Strategy
for
Data](https://digital-strategy.ec.europa.eu/en/policies/strategy-data)[.](https://digital-strategy.ec.europa.eu/en/policies/strategy-data)
The 2022 edition had the active support of over 20 partners, representing open
data stakeholders from both inside and outside the EU institutions.
