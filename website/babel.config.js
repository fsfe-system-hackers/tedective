// SPDX-FileCopyrightText: 2022 Free Software Foundation Europe <contact@fsfe.org>
//
// SPDX-License-Identifier: CC0-1.0

module.exports = {
  presets: [require.resolve('@docusaurus/core/lib/babel/preset')],
}
