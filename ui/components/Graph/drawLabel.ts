// SPDX-FileCopyrightText: 2023 Free Software Foundation Europe <contact@fsfe.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

import { TEDectiveNode } from '@/pages'
import assert from 'assert'
import wrap from 'word-wrap'
import { initialVisuals } from '../config'
import getNodeSize from './getNodeSize'
import { getOpacity } from './getOpacity'
import { getThemeColor } from './getThemeColor'
import hexToRGBA from './hexToRGBA'

const getNodeTitle = (node: TEDectiveNode) => {
  switch (node.type) {
    case 'organization':
      return node.data.name!
    case 'release':
      assert(node.data.tender)
      const title = node.data.tender.title!
      const newTitle = title.substring(0, title.indexOf('['))
      return newTitle
  }
}

export default function drawLabel({
  node,
  theme,
  hoverNode,
  highlightedNodes,
  ctx,
  globalScale,
  opacity,
  visuals,
}: {
  node: TEDectiveNode
  theme: any
  hoverNode: TEDectiveNode | null
  highlightedNodes: Set<TEDectiveNode>
  ctx: any
  globalScale: number
  opacity: number
  visuals: typeof initialVisuals
}) {
  const nodeS = Math.cbrt(
    (visuals.nodeRel *
      getNodeSize({
        node,
        opacity,
        visuals,
      })) /
      Math.pow(globalScale, visuals.nodeZoomSize)
  )

  const isHighlighty = highlightedNodes.has(node)

  const nodeTitle = getNodeTitle(node)

  const label = nodeTitle.substring(0, visuals.labelLength)

  const fontSize =
    (isHighlighty
      ? visuals.labelFontSize * visuals.highlightLabelFontSize
      : visuals.labelFontSize) /
    Math.cbrt(Math.pow(globalScale, visuals.nodeZoomSize))

  const textOpacity = getOpacity(visuals, hoverNode, opacity, isHighlighty)

  // draw label text
  ctx.textAlign = 'center'
  ctx.textBaseline = 'middle'
  const labelTextColor = getThemeColor(visuals.labelTextColor, theme)
  const labelText = hexToRGBA(labelTextColor, textOpacity)
  ctx.fillStyle = labelText
  ctx.font = `${fontSize}px Sans-Serif`
  const wordsArray = wrap(label, { width: visuals.labelWordWrap }).split('\n')

  const truncatedWords =
    nodeTitle.length > visuals.labelLength
      ? [...wordsArray.slice(0, -1), `${wordsArray.slice(-1)}...`]
      : wordsArray

  const highlightedNodeOffset =
    hoverNode && hoverNode === node ? 1 + 0.3 * opacity : 1
  truncatedWords.forEach((word, index) => {
    ctx.fillText(
      word,
      node.x!,
      node.y! +
        highlightedNodeOffset * nodeS * 8 +
        visuals.labelLineSpace * fontSize * index
    )
  })
}
