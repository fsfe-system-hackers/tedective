// SPDX-FileCopyrightText: 2023 Free Software Foundation Europe <contact@fsfe.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

import { TEDectiveLink, TEDectiveNode } from '@/pages'
import { initialVisuals } from '../config'
import { isLinkRelatedToNode } from './isLinkRelatedToNode'
import { getOpacity } from './getOpacity'
import { getThemeColor } from './getThemeColor'
import hexToRGBA from './hexToRGBA'

export default function getLinkColor({
  link,
  theme,
  highlightedNodes,
  hoverNode,
  opacity,
  visuals,
}: {
  link: TEDectiveLink
  theme: any
  highlightedNodes: Set<TEDectiveNode>
  hoverNode: TEDectiveNode | null
  opacity: number
  visuals: typeof initialVisuals
}) {
  const needsHighlighting = isLinkRelatedToNode(link, highlightedNodes)

  const linkColor = getThemeColor(visuals.linkColor, theme)

  const nodeOpacity = getOpacity(visuals, hoverNode, opacity, needsHighlighting)

  return hexToRGBA(linkColor, nodeOpacity)
}
