// SPDX-FileCopyrightText: 2023 Free Software Foundation Europe <contact@fsfe.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

import { Organization } from '@/models/Organization';
import {
  Box,
  Button, Highlight, Input,
  SimpleGrid, Text
} from '@chakra-ui/react';
import { instantMeiliSearch } from '@meilisearch/instant-meilisearch';
import { useCallback, useState } from 'react';
import { Configure, InstantSearch, useHits, useSearchBox } from 'react-instantsearch-hooks-web';
import wrap from 'word-wrap';


const meiliClient = instantMeiliSearch(
  'https://search.tedective.org/',
  // This key only has search permissions
  '5baf4cb5d53905e71e2303b259a751ff654a9796aed78851bbb15fb3eff7e315'
)

// Don't launch a search on empty input
// https://www.algolia.com/doc/guides/building-search-ui/going-further/conditional-requests/js/#detecting-empty-search-requests
const searchClient = {
  ...meiliClient,
  search(requests: any) {
    if (requests.every(({ params }: { params: any }) => !params.query)) {
      return Promise.resolve({
        results: requests.map(() => ({
          hits: [],
          nbHits: 0,
          nbPages: 0,
          page: 0,
          processingTimeMS: 0,
          hitsPerPage: 0,
          exhaustiveNbHits: false,
          query: '',
          params: '',
        })),
      });
    }

    return meiliClient.search(requests);
  },
};

export interface SearchBoxProps {
  setSearchedOrg: any
}

export function SearchBox({ setSearchedOrg }: SearchBoxProps) {
  // corresponds to the organization of the initial releaes shown
  const [value, setValue] = useState('Gemeinde Unterföhring')
  const memoizedSearch = useCallback((query: any, search: any) => {
    search(query);
  }, []);
  const { refine, clear } = useSearchBox({
    queryHook: memoizedSearch,
  });
  const handleChange = (event: any) => {
    setValue(event.target.value)
    refine(event.target.value);
  };
  // Center the Searchbar vertically
  // https://github.com/chakra-ui/chakra-ui/discussions/3822
  return (
    <Box
      position="absolute"
      left="50%"
      transform="translate(-50%)"
      w={64}
      mt={2}
      zIndex={4}
    >
      <Input
        onChange={handleChange}
        value={value}
        placeholder="Organization"
        bgColor="white"
        mb={2}
      />
      <SearchResults
        {...{
          clear,
          value,
          setValue,
          setSearchedOrg,
        }}
      />
    </Box>
  );

}

export interface SearchResultsProps {
  clear: any
  value: string
  setValue: any
  setSearchedOrg: any
}

export function SearchResults({ clear, value, setValue, setSearchedOrg }: SearchResultsProps) {
  const { hits } = useHits();
  return (
    <SimpleGrid
      columns={1}
      bgColor="white"
    >
      {hits.map((hit) => {
        return (
          <Box key={hit.id as string}>
            <SearchHit {...{
              hit,
              clear,
              value,
              setValue,
              setSearchedOrg,
            }} />
          </Box>
        )
      }
      )}
    </SimpleGrid>
  )
}

export interface SearchHitProps {
  hit: any
  clear: any
  value: string
  setValue: any
  setSearchedOrg: any
}

export function SearchHit({ hit, clear, value, setValue, setSearchedOrg }: SearchHitProps) {
  const org = hit as Organization
  const wordsArray = wrap(org.name, { width: 30 }).split('\n')
  return (
    <Box
      w={64}
      maxW={64}
    >
      <Button
        variant="ghost"
        w="100%"
        h="auto"
        display="block"
        p={2}

        onClick={((e) => {
          setValue(org.name)
          setSearchedOrg(org)
          clear()
        })}
      >
        {
          wordsArray.map((s, i) => {
            return (
              <Box key={i}>
                <Text
                  fontSize="sm"
                  textAlign="left"
                >
                  <Highlight
                    styles={{ bg: 'orange.100' }}
                    query={value}>
                    {s}
                  </Highlight>
                </Text>
              </Box>
            )
          })
        }
      </Button>
    </Box >
  )
}

export interface SearchbarProps {
  setSearchedOrg: any
}

export const Searchbar = ({ setSearchedOrg }: SearchbarProps) => {
  return (
    <InstantSearch
      indexName="organizations"
      searchClient={searchClient}
    >
      <Configure
        hitsPerPage={10}
        distinct={true}
      />
      <SearchBox
        setSearchedOrg={setSearchedOrg}
      />
    </InstantSearch>
  )
}

export default Searchbar
