// SPDX-FileCopyrightText: 2023 Free Software Foundation Europe <contact@fsfe.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

import {
  Box,
  RangeSlider,
  RangeSliderFilledTrack,
  RangeSliderThumb,
  RangeSliderTrack,
  Text,
  Tooltip,
} from '@chakra-ui/react'
import { InfoTooltip } from './InfoTooltip'

export interface RangeSliderWithInfoProps {
  min?: number
  max?: number
  step?: number
  value: [number, number]
  onChangeEnd: (args: [number, number]) => void
  label: string
  infoText?: string
}

export const RangeSliderWithInfo = ({
  min = 0,
  max = 10,
  step = 0.1,
  value = [0, 10],
  ...rest
}: RangeSliderWithInfoProps) => {
  const { onChangeEnd, label, infoText } = rest
  return (
    <Box key={label} pt={1} pb={2}>
      <Box display="flex" alignItems="flex-end" mb={2}>
        <Text>{label}</Text>
        {infoText && <InfoTooltip infoText={infoText} />}
      </Box>
      <RangeSlider
        aria-label={['min', 'max']}
        defaultValue={value}
        onChangeEnd={onChangeEnd}
        min={min}
        max={max}
        step={step}
      >
        <RangeSliderTrack>
          <RangeSliderFilledTrack />
        </RangeSliderTrack>
        <Tooltip label={value[0]}>
          <RangeSliderThumb index={0} />
        </Tooltip>
        <Tooltip label={value[1]}>
          <RangeSliderThumb index={1} />
        </Tooltip>
      </RangeSlider>
    </Box>
  )
}
