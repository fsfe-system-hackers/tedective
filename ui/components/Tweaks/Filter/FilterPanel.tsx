// SPDX-FileCopyrightText: 2023 Free Software Foundation Europe <contact@fsfe.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

import { Box, StackDivider, VStack } from '@chakra-ui/react'
import { initialFilter } from '../../config'
import { RangeSliderWithInfo } from '../RangeSliderWithInfo'

export interface FilterPanelProps {
  filter: typeof initialFilter
  setFilter: any
}

export const FilterPanel = (props: FilterPanelProps) => {
  const { filter, setFilter } = props

  const minYear = filter.minMaxYears[0]
  const maxYear = filter.minMaxYears[1]

  return (
    <Box>
      <VStack
        spacing={2}
        justifyContent="flex-start"
        divider={<StackDivider borderColor="gray.500" />}
        align="stretch"
        paddingLeft={7}
        color="gray.800"
      >
        <RangeSliderWithInfo
          label="Date range"
          value={filter.dateRange as [number, number]}
          min={minYear}
          max={maxYear}
          step={1}
          onChangeEnd={(dR) =>
            setFilter({ ...filter, dateRange: [dR[0], dR[1]] })
          }
        />
      </VStack>
    </Box>
  )
}

export default FilterPanel
