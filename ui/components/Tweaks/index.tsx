// SPDX-FileCopyrightText: 2023 Free Software Foundation Europe <contact@fsfe.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

import { CloseIcon, SettingsIcon } from '@chakra-ui/icons'
import {
  Accordion,
  AccordionButton,
  AccordionIcon,
  AccordionItem,
  AccordionPanel,
  Box,
  Heading,
  IconButton,
} from '@chakra-ui/react'
import { useState } from 'react'
import Scrollbars from 'react-custom-scrollbars-2'

import { initialFilter, initialPhysics, initialVisuals } from '../config'
import { FilterPanel } from './Filter/FilterPanel'
import { PhysicsPanel } from './Physics/PhysicsPanel'
import { VisualsPanel } from './Visual/VisualsPanel'

export interface TweakProps {
  filter: typeof initialFilter
  setFilter: any
  physics: typeof initialPhysics
  setPhysics: any
  visuals: typeof initialVisuals
  setVisuals: any
}

export const Tweaks = ({
  filter,
  setFilter,
  physics,
  setPhysics,
  visuals,
  setVisuals,
}: TweakProps) => {
  const [showTweaks, setShowTweaks] = useState(false)

  // XXX
  const highlightColor = 'purple'

  return !showTweaks ? (
    <Box
      position="absolute"
      zIndex={10}
      marginTop={1}
      marginLeft={0}
      display={showTweaks ? 'none' : 'block'}
    >
      <IconButton
        variant="subtle"
        aria-label="Settings"
        icon={<SettingsIcon />}
        onClick={() => setShowTweaks(true)}
      />
    </Box>
  ) : (
    <Box
      position="absolute"
      bg="white"
      w="xs"
      marginTop={2}
      marginLeft={2}
      borderRadius="lg"
      paddingBottom={5}
      zIndex={10}
      boxShadow="xl"
      maxH={'95vh'}
      fontSize="sm"
    >
      <Box
        display="flex"
        justifyContent="space-between"
        alignItems="center"
        paddingRight={2}
        paddingTop={1}
      >
        {/* <Tooltip label={'Switch to ' + threeDim ? '2D' : '3D' + ' view'}>
          <Button onClick={() => setThreeDim(!threeDim)} variant="subtle" zIndex="overlay">
            {threeDim ? '3D' : '2D'}
          </Button>
        </Tooltip> */}
        <Box display="flex" alignItems="center">
          {/* <Tooltip label="Reset settings to defaults">
            <IconButton
              aria-label="Reset Defaults"
              icon={<RepeatClockIcon />}
              onClick={() => {
                setVisuals(initialVisuals)
                setFilter(initialFilter)
                setMouse(initialMouse)
                setPhysics(initialPhysics)
                setBehavior(initialBehavior)
                setColoring(initialColoring)
                setHighlightColor('purple.500')
                setLocal(initialLocal)
              }}
              variant="subtle"
              size="sm"
            />
          </Tooltip> */}
          <IconButton
            size="sm"
            icon={<CloseIcon />}
            aria-label="Close Tweak Panel"
            variant="subtle"
            onClick={() => setShowTweaks(false)}
          />
        </Box>
      </Box>
      <Scrollbars
        autoHeight
        autoHeightMax={0.85 * globalThis.innerHeight}
        autoHide
        renderThumbVertical={({ style, ...props }) => (
          <Box
            {...props}
            style={{
              ...style,
              borderRadius: 10,
            }}
            bg={highlightColor}
          />
        )}
      >
        <Accordion allowMultiple allowToggle color="black">
          <AccordionItem>
            <AccordionButton>
              <AccordionIcon marginRight={2} />
              <Heading size="sm">Filter</Heading>
            </AccordionButton>
            <AccordionPanel>
              <FilterPanel filter={filter} setFilter={setFilter} />
            </AccordionPanel>
          </AccordionItem>
          <AccordionItem>
            <AccordionButton>
              <Box display="flex">
                <AccordionIcon marginRight={2} />
                <Heading size="sm">Physics</Heading>
              </Box>
            </AccordionButton>
            <AccordionPanel>
              <PhysicsPanel physics={physics} setPhysics={setPhysics} />
            </AccordionPanel>
          </AccordionItem>
          <AccordionItem>
            <AccordionButton>
              <AccordionIcon marginRight={2} />
              <Heading size="sm">Visual</Heading>
            </AccordionButton>
            <AccordionPanel>
              <VisualsPanel
                visuals={visuals}
                setVisuals={setVisuals}
                highlightColor={highlightColor}
              />
            </AccordionPanel>
          </AccordionItem>
        </Accordion>
      </Scrollbars>
    </Box>
  )
}
