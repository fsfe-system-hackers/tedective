// SPDX-FileCopyrightText: 2023 Free Software Foundation Europe <contact@fsfe.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

import type { AppProps } from 'next/app'
import { ChakraProvider, extendTheme, type ThemeConfig } from '@chakra-ui/react'
import { SWRConfig } from 'swr'

const config: ThemeConfig = {
  initialColorMode: 'light',
  useSystemColorMode: false,
}

const theme = extendTheme({ config })

export default function App({ Component, pageProps }: AppProps) {
  return (
    <ChakraProvider theme={theme}>
      <SWRConfig>
        <Component {...pageProps} />
      </SWRConfig>
    </ChakraProvider>
  )
}
