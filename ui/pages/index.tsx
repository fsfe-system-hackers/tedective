// SPDX-FileCopyrightText: 2023 Free Software Foundation Europe <contact@fsfe.org>
//
// SPDX-License-Identifier: GPL-3.0-or-later

import { readFile } from 'node:fs/promises'
import {
  Box,
  Flex,
  Icon,
  IconButton,
  Tooltip,
  useDisclosure,
  AbsoluteCenter,
  Spinner,
  VStack,
  Text,
} from '@chakra-ui/react'
import { ChevronRightIcon, ChevronLeftIcon } from '@chakra-ui/icons'
import { GraphData, LinkObject, NodeObject } from 'force-graph'
import { GetStaticProps, InferGetStaticPropsType } from 'next'
import dynamic from 'next/dynamic'
import Head from 'next/head'
import useSWRInfinite, { SWRInfiniteKeyLoader } from 'swr/infinite'
import { useState, useEffect, useMemo } from 'react'
import assert from 'assert'

import { components } from '../api'
import Sidebar from '../components/Sidebar'
import { Tweaks } from '../components/Tweaks'
import Searchbar from '../components/Searchbar'
import {
  initialFilter,
  initialVisuals,
  initialPhysics,
} from '../components/config'
import { Release } from '../models/Release'
import { Organization } from '../models/Organization'

type ReleaseRead = components['schemas']['ReleaseRead']

export class HTTPValidationError extends Error {
  constructor(info: components['schemas']['HTTPValidationError']) {
    super('HTTP Validation Error')
    this.info = info
  }
  info: components['schemas']['HTTPValidationError']
  toString() {
    // TODO
    return super.toString()
  }
}

export interface TEDectiveGraph extends GraphData {
  nodes: TEDectiveNode[]
  links: TEDectiveLink[]
}

export type TEDectiveNode = ReleaseNode | OrganizationNode
export interface ReleaseNode extends NodeObject {
  type: 'release'
  data: Release
}
export interface OrganizationNode extends NodeObject {
  type: 'organization'
  data: Organization
}

export interface TEDectiveLink extends LinkObject {
  source: TEDectiveNode
  target: TEDectiveNode
}

export type LinksByOcid = { [ocid: string]: TEDectiveLink[] }
export type OcidsByOrgId = { [orgId: string]: Set<string> }
export type OrgNodeById = { [orgId: string]: OrganizationNode }
export type RelNodeById = { [relId: string]: ReleaseNode }

// react-force-graph fails on import when server-rendered
// https://github.com/vasturiano/react-force-graph/issues/155
// https://github.com/vasturiano/react-force-graph/issues/164
const Graph = dynamic(() => import('../components/Graph'), {
  ssr: false,
})

//const fetcher = TypedFetcher.for<paths>()

const fetcher = async (url: string) => {
  const res = await fetch(url)

  if (!res.ok) {
    const info = await res.json()
    const error = new HTTPValidationError(info)
    console.error(info)
    throw error
  }

  return await res.json()
}

// number of releases per request
const LIMIT = 100

export default function Home({
  initReleasesData,
}: InferGetStaticPropsType<typeof getStaticProps>) {
  const [releasesData, setReleasesData] = useState<ReleaseRead[][]>([
    initReleasesData,
  ])
  const [previewNode, setPreviewNode] = useState<TEDectiveNode | null>(null)
  const [filter, setFilter] = useState<typeof initialFilter>(initialFilter)
  const [physics, setPhysics] = useState<typeof initialPhysics>(initialPhysics)
  const [visuals, setVisuals] = useState<typeof initialVisuals>(initialVisuals)
  const [searchedOrg, setSearchedOrg] = useState<Organization | null>(null)
  const { isOpen, onToggle } = useDisclosure()

  const getKey: SWRInfiniteKeyLoader = (index, previousPageData) => {
    // no search input
    if (!searchedOrg) return null
    // reached the end
    if (previousPageData && !previousPageData.length) return null
    const constructedUrl = [
      `/api/releases?org_id=${searchedOrg.id}`,
      `is_buyer=${searchedOrg.roles.includes('buyer')}`,
      `offset=${index * LIMIT}`,
      `limit=${LIMIT}`,
    ]
    return constructedUrl.join('&')
  }

  const { data, error, size, setSize, isLoading } = useSWRInfinite(
    getKey,
    fetcher,
    {
      revalidateFirstPage: false,
    }
  )

  const isLoadingMore =
    isLoading || (size > 0 && data && typeof data[size - 1] === 'undefined')
  const isEmpty = data?.[0]?.length === 0
  const isReachingEnd =
    isEmpty || (data && data[data.length - 1]?.length < LIMIT)

  if (isReachingEnd && data !== releasesData) {
    setReleasesData(data)
  }

  // fetch all the releases for an org
  useEffect(() => {
    if (isLoadingMore) return
    if (error) return
    if (!isReachingEnd) {
      setSize(size + 1)
    }
  }, [size, data, error, isLoadingMore, isReachingEnd, setSize])

  const releases = useMemo(
    () => Release.parse(releasesData.flat()),
    [releasesData]
  )

  const releasesByOcid = useMemo(() => {
    const releasesByOcid: { [ocid: string]: Release[] } = {}

    for (const rel of releases) {
      if (!releasesByOcid[rel.ocid]) {
        releasesByOcid[rel.ocid] = []
      }
      releasesByOcid[rel.ocid].push(rel)
    }

    return releasesByOcid
  }, [releases])

  // update date range
  useEffect(() => {
    const minMaxDateTimes = [NaN, NaN]

    for (const rel of releases) {
      const minDate = minMaxDateTimes[0]
      if (Number.isNaN(minDate)) {
        minMaxDateTimes[0] = rel.date.getTime()
      } else {
        minMaxDateTimes[0] =
          minDate > rel.date.getTime() ? rel.date.getTime() : minDate
      }
      const maxDate = minMaxDateTimes[1]
      if (Number.isNaN(maxDate)) {
        minMaxDateTimes[1] = rel.date.getTime()
      } else {
        minMaxDateTimes[1] =
          maxDate < rel.date.getTime() ? rel.date.getTime() : maxDate
      }
    }

    const minMaxYears = minMaxDateTimes.map((dt) =>
      Number.isNaN(dt) ? dt : new Date(dt).getFullYear()
    )

    setFilter({ ...filter, dateRange: minMaxYears, minMaxYears })
  }, [releases])

  const [filteredReleases, filteredReleasesByOcid] = useMemo(() => {
    const filteredReleasesByOcid: { [ocid: string]: Release[] } = Object.keys(
      releasesByOcid
    )
      .filter((ocid) => {
        const releases = releasesByOcid[ocid]
        if (filter.dateRange.includes(NaN)) return true
        const includesRelease = releases.find((rel) => {
          const year = rel.date.getFullYear()
          return filter.dateRange[0] <= year && year <= filter.dateRange[1]
        })
        if (includesRelease) return true
        return false
      })
      .reduce(
        (acc, ocid) => Object.assign(acc, { [ocid]: releasesByOcid[ocid] }),
        {}
      )
    return [
      Object.values(filteredReleasesByOcid).flat(),
      filteredReleasesByOcid,
    ]
  }, [releasesByOcid, filter])

  const [graphData, linksByOcid, ocidsByOrgId] = useMemo(() => {
    const graphData: TEDectiveGraph = { nodes: [], links: [] }

    const orgNodeById: OrgNodeById = {}
    const relNodeById: RelNodeById = {}

    const maybeAddNode = (data: Release | Organization) => {
      if (data instanceof Organization) {
        const orgId = data.id
        if (!orgNodeById[orgId]) {
          orgNodeById[orgId] = {
            id: orgId,
            type: 'organization',
            data,
          }
          graphData.nodes.push(orgNodeById[orgId])
        }
        return orgNodeById[orgId]
      } else {
        const relId = data.id
        if (!relNodeById[relId]) {
          relNodeById[relId] = {
            id: relId,
            type: 'release',
            data,
          }
          graphData.nodes.push(relNodeById[relId])
        }
        return relNodeById[relId]
      }
    }

    const ocidsByOrgId: OcidsByOrgId = {}

    const addOrgId = (node: TEDectiveNode, ocid: string) => {
      if (node.type === 'organization') {
        const orgId = node.data.id
        if (!ocidsByOrgId[orgId]) {
          ocidsByOrgId[orgId] = new Set<string>()
        }
        ocidsByOrgId[orgId].add(ocid)
      }
    }

    const linksByOcid: LinksByOcid = {}

    const addLink = (link: TEDectiveLink, ocid: string) => {
      if (!linksByOcid[ocid]) {
        linksByOcid[ocid] = []
      }
      ;[link.source, link.target].forEach((node) => addOrgId(node, ocid))
      linksByOcid[ocid].push(link)
      graphData.links.push(link)
    }

    for (const [ocid, releases] of Object.entries(filteredReleasesByOcid)) {
      // Assumption: `buyer` is the same organization for all `releases`
      const buyer: Organization = releases[0].buyer

      const planningReleases = releases.filter((rel) =>
        rel.tag.includes('planning')
      )

      if (planningReleases.length !== 0) {
        for (const rel of planningReleases) {
          addLink(
            {
              source: maybeAddNode(buyer),
              target: maybeAddNode(rel),
            },
            ocid
          )
        }
      }

      const tenderReleases = releases.filter((rel) =>
        rel.tag.includes('tender')
      )

      if (tenderReleases.length !== 0) {
        for (const tR of tenderReleases) {
          if (planningReleases.length !== 0) {
            for (const pR of planningReleases) {
              addLink(
                {
                  source: maybeAddNode(pR),
                  target: maybeAddNode(tR),
                },
                ocid
              )
            }
          } else {
            addLink(
              {
                source: maybeAddNode(buyer),
                target: maybeAddNode(tR),
              },
              ocid
            )
          }
        }
      }

      const awardReleases = releases.filter((rel) => rel.tag.includes('award'))

      if (awardReleases.length !== 0) {
        // award releases do not necessarily have tenders associated with them (ocds-jyvdv7-613326db-e0f8-5122-bfeb-368fa3898fbe)
        // assert(tenderReleases.length !== 0)
        for (const aR of awardReleases) {
          if (tenderReleases.length !== 0) {
            for (const tR of tenderReleases) {
              addLink(
                {
                  source: maybeAddNode(tR),
                  target: maybeAddNode(aR),
                },
                ocid
              )
            }
          } else {
            for (const aR of awardReleases) {
              addLink(
                {
                  source: maybeAddNode(buyer),
                  target: maybeAddNode(aR),
                },
                ocid
              )
            }
          }
        }
        for (const aR of awardReleases) {
          assert(aR.awards?.length !== 0)
          for (const award of aR.awards!) {
            // can have unsuccessful awards, e.g. award ID: 3ae81d9b-562e-4c7b-afe7-4be9bfcc0a32
            // assert(award.suppliers?.length !== 0)
            for (const sup of award.suppliers!) {
              addLink(
                {
                  source: maybeAddNode(aR),
                  target: maybeAddNode(sup),
                },
                ocid
              )
            }
          }
        }
      }
    }

    return [graphData, linksByOcid, ocidsByOrgId]
  }, [filteredReleasesByOcid])

  return (
    <>
      <Head>
        <title>TEDective UI</title>
      </Head>
      <Box
        display="flex"
        alignItems="flex-start"
        flexDirection="row"
        height="100vh"
        overflow="clip"
      >
        <Tweaks
          {...{
            filter,
            setFilter,
            physics,
            setPhysics,
            visuals,
            setVisuals,
          }}
        />
        <Searchbar
          {...{
            setSearchedOrg,
          }}
        />
        <AbsoluteCenter>
          {isLoading ? (
            <LoadingSpinner />
          ) : error ? (
            <Text>{error.message}</Text>
          ) : (
            graphData && (
              <Graph
                {...{
                  graphData,
                  linksByOcid,
                  ocidsByOrgId,
                  setPreviewNode,
                  physics,
                  visuals,
                }}
              />
            )
          )}
        </AbsoluteCenter>
        <Box position="relative" width="100%">
          <Flex className="headerBar" h={10} flexDir="column">
            <Flex alignItems="center" justifyContent="end">
              <Tooltip label={isOpen ? 'Close sidebar' : 'Open sidebar'}>
                <IconButton
                  icon={
                    <Icon as={isOpen ? ChevronRightIcon : ChevronLeftIcon} />
                  }
                  aria-label="Toggle sidebar"
                  onClick={onToggle}
                />
              </Tooltip>
            </Flex>
          </Flex>
        </Box>
        <Box position="relative" zIndex={4}>
          {graphData && (
            <Sidebar
              {...{
                previewNode,
                isOpen,
                onToggle,
              }}
            />
          )}
        </Box>
      </Box>
    </>
  )
}

const LoadingSpinner = () => {
  return (
    <VStack>
      <Spinner />
      <Text>Loading Graph...</Text>
    </VStack>
  )
}

export const getStaticProps: GetStaticProps<{
  initReleasesData: ReleaseRead[]
}> = async () => {
  /* const res = await fetch(`${baseUrl}/releases_for_org/${orgId}`);
   * const releases = await res.json(); */
  const file = await readFile('./data/initial_releases.json', {
    encoding: 'utf8',
  })
  const releasesData = JSON.parse(file)
  return {
    props: {
      initReleasesData: releasesData,
    },
  }
}
