# SPDX-FileCopyrightText: 2022 Free Software Foundation Europe <contact@fsfe.org>
#
# SPDX-License-Identifier: CC0-1.0

##
# TEDective Makefile
# This Makefile contains make rules to ease working with TEDective

.DEFAULT_GOAL := help

GREEN  = $(shell tput -Txterm setaf 2)
WHITE  = $(shell tput -Txterm setaf 7)
YELLOW = $(shell tput -Txterm setaf 3)
RESET  = $(shell tput -Txterm sgr0)

COMPOSE := docker compose

HELPME = \
	%help; \
	while(<>) { push @{$$help{$$2 // 'options'}}, [$$1, $$3] if /^([a-zA-Z\-\.]+)\s*:.*\#\#(?:@([a-zA-Z\-]+))?\s(.*)$$/ }; \
	for (sort keys %help) { \
	print "${WHITE}$$_:${RESET}\n"; \
	for (@{$$help{$$_}}) { \
	$$sep = " " x (20 - length $$_->[0]); \
	print "  ${YELLOW}$$_->[0]${RESET}$$sep${GREEN}$$_->[1]${RESET}\n"; \
	}; \
	print "\n"; }

help:
	@perl -e '$(HELPME)' $(MAKEFILE_LIST)
.PHONY: help


# -- Development (general)
dev: ##@development Start all containers and show their logs.
	@USER_ID=$${USER_ID:-`id -u`} GROUP_ID=$${GROUP_ID:-`id -g`} cd ./api; docker compose up
.PHONY: dev

dev.up: ##@development Start all containers and detach.
	@USER_ID=$${USER_ID:-`id -u`} GROUP_ID=$${GROUP_ID:-`id -g`} cd ./api; docker compose up --detach --no-recreate
.PHONY: dev.up

dev.up.test: ##@development Start containers needed for testing and detach.
	@USER_ID=$${USER_ID:-`id -u`} GROUP_ID=$${GROUP_ID:-`id -g`} cd ./api; docker compose -f docker-compose.test.yml up --detach
.PHONY: dev.up.test

dev.down: ##@development Stop all containers.
	@USER_ID=$${USER_ID:-`id -u`} GROUP_ID=$${GROUP_ID:-`id -g`} cd ./api; docker compose down --remove-orphans
.PHONY: dev.down

dev.kill: ##@development Kill and subsequently remove all containers.
	@USER_ID=$${USER_ID:-`id -u`} GROUP_ID=$${GROUP_ID:-`id -g`} cd ./api; docker compose kill --remove-orphans
.PHONY: dev.kill

dev.rebuild: dev.down ##@development Stop and restart containers (but rebuild them this time)
	@USER_ID=$${USER_ID:-`id -u`} GROUP_ID=$${GROUP_ID:-`id -g`} cd ./api; docker compose up --build --force-recreate
.PHONY: dev.rebuild

# -- API development
api.test: dev.up.test ##@api Test the TEDective API
	@USER_ID=$${USER_ID:-`id -u`} GROUP_ID=$${GROUP_ID:-`id -g`} cd ./api; docker compose exec -it tedective-api-test /bin/sh -c "pytest ./tests"
.PHONY: api.test

# Not working fn
api.partial.ingest: dev.up ##@api Ingest only first 200 TED XMLs from each month into TerminusDB
	@USER_ID=$${USER_ID:-`id -u`} GROUP_ID=$${GROUP_ID:-`id -g`} cd ./api; docker compose exec -it tedective-api /bin/sh -c "python ./tedective_api/ingest.py True"

api.ingest: dev.up ##@api Ingest TED XMLs into TerminusDB
	@USER_ID=$${USER_ID:-`id -u`} GROUP_ID=$${GROUP_ID:-`id -g`} cd ./api; docker compose exec -it tedective-api /bin/sh -c "python ./tedective_api/ingest.py False"
.PHONY: api.ingest

# -- Website development
website.up: ##@website Start the website in development mode
	@USER_ID=$${USER_ID:-`id -u`} GROUP_ID=$${GROUP_ID:-`id -g`} cd ./website; docker compose up --detach --remove-orphans
.PHONY: website.up

website.down: ##@website Stop the website running in development mode
	@USER_ID=$${USER_ID:-`id -u`} GROUP_ID=$${GROUP_ID:-`id -g`} cd ./website; docker compose down --remove-orphans
.PHONY: website.down

# -- Database operations
db.recreate: dev.up ##@db Recreate empty TerminusDB with layout according to ./api/tedective/schema.py
	@USER_ID=$${USER_ID:-`id -u`} GROUP_ID=$${GROUP_ID:-`id -g`} cd ./api; docker compose exec -it tedective-api /bin/sh -c "python ./tedective_api/database.py"
.PHONY: db.recreate

# -- Setting up the development
dev.install: dev.up db.recreate api.test ##@development Set up the complete development environment
.PHONY: dev.install

#db.dump: dev.up ##@db Create PostgreSQL dump in ./api/out/db_dumps
#	@echo "Creating PostgreSQL dump..."
#	@/usr/bin/env bash ./api/scripts/psql_dump.sh
#	@echo "Done!"
#.PHONY: db.dump
#
#db.push: ##@db Restore most recent PostgreSQL dump to production server.
#	@echo "Restoring most recent PostgreSQL dump to production server..."
#	@/usr/bin/env PGPASSWORD=${POSTGRES_PASSWORD} bash ./api/scripts/psql_push.sh
#	@echo "Done!"
#.PHONY: db.push

##
# end
